/*
 * rasterdata.cpp
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#include "rasterdata.h"

/*
RasterData::RasterData(Data*** in, int b, int r, int c, float rrf, int roff, float crf, int coff) {

	int i,j,k;

	bands = b;
	rows = r;
	cols = c;
	ras = new Data**[b];

	ras = new Data**[bands];
	for (k=0;k<bands;k++) {
		ras[k] = new Data*[rows];
		for (i=0;i<rows;i++) {
			ras[k][i] = new Data[cols];
			for (j=0;j<cols;j++) ras[k][i][j] = in[k][i][j];
		}
	}

	rResolutionFactor = 1/rrf;
	cResolutionFactor = crf == -1.0 ? rResolutionFactor : 1/crf;

	rowOffset = roff;
	colOffset = coff == -1 ? roff : coff;

	for (k=0;k<bands;k++) selectedBands.push_back(k);

	weights = 0;

	//generate row/col indices

}
*/

RasterData::RasterData(Data*** in, int b, int r, int c, int oR, int oC, int* row_idx, int* col_idx) {

	int i,j,k;

	bands = b;
	rows = r;
	cols = c;
	ras = new Data**[b];

	ras = new Data**[bands];
	for (k=0;k<bands;k++) {
		ras[k] = new Data*[rows];
		for (i=0;i<rows;i++) {
			ras[k][i] = new Data[cols];
			for (j=0;j<cols;j++) ras[k][i][j] = in[k][i][j];
		}
	}

	rResolutionFactor = -1;
	cResolutionFactor = -1;

	rowOffset = -1;
	colOffset = -1;

	oRows = oR;
	oCols = oC;

	rowIndices = new int[oR];
	for (i=0;i<oR;i++) rowIndices[i] = row_idx[i];
	colIndices = new int[oC];
	for (i=0;i<oC;i++) colIndices[i] = col_idx[i];

	for (k=0;k<bands;k++) selectedBands.push_back(k);

	weights = 0;

}

RasterData::~RasterData() {

	int k,i;
	for (k=0;k<bands;k++) {
		for (i=0;i<rows;i++) {
			delete [] ras[k][i];
		}
		delete [] ras[k];
	}

	if (weights) {
		for (i=0;i<rows;i++) delete [] weights[i];
		delete [] weights;
	}

}

void RasterData::selectBands(vector<int> sb) {

	size_t t;
	selectedBands.clear();
	for (t=0;t<sb.size();t++) selectedBands.push_back(sb[t]);


}

void RasterData::deselectBands() {

	int t;
	selectedBands.clear();
	for (t=0;t<bands;t++) selectedBands[t] = t;

}

void RasterData::setWeights(float** w) {

	int i,j;
	weights = new float*[rows];
	for (i=0;i<rows;i++) {
		weights[i] = new float[cols];
		for (j=0;j<cols;j++) weights[i][j] = w[i][j];
	}

}
