/*
 * main.cpp
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#define _hypot hypot
#include "Python.h"
#if PY_MAJOR_VERSION >= 3
#define PY3K
#endif

#include <arrayobject.h>

#include "dataconverter.h"
#include "rasterstats.h"
#include "rasterstats1d.h"

using namespace std;

static PyObject *
obiatools_version(PyObject *self, PyObject *args) {

    PySys_WriteStdout("Hello from OBIATOOLS! Version 0.0.1a");
    return Py_None;

}

//V2
static PyObject *
obiatools_zonalstats(PyObject *self, PyObject *args) {

    int t,k,s;
    int B,R,C,oR,oC,nStats,nObj;
    PyArrayObject *ol, *ras, *ridx, *cidx, *stats;
    PyObject *w = 0, *nv = 0;
    PyArg_ParseTuple(args,"O!O!O!O!O!|OO", &PyArray_Type, &ol, &PyArray_Type, &ras, &PyArray_Type, &ridx, &PyArray_Type, &cidx, &PyArray_Type, &stats, &nv, &w);

    npy_intp nRasDims = PyArray_NDIM(ras);
    npy_intp *rasDims = PyArray_DIMS(ras);
    if (nRasDims == 2) {
        B = 1; R = rasDims[0]; C = rasDims[1];
    } else {
        B = rasDims[0]; R = rasDims[1]; C = rasDims[2];
    }

    npy_intp *olDims = PyArray_DIMS(ol);
    oR = olDims[0]; oC = olDims[1];

    Data* ras_arr = (Data*)PyArray_DATA(ras);
    Label* ol_arr = (Label*)PyArray_DATA(ol);

    int* row_idx = (int*)PyArray_DATA(ridx);
    int* col_idx = (int*)PyArray_DATA(cidx);

    npy_intp *statsDims = PyArray_DIMS(stats);
    int* stats_vec = (int*)PyArray_DATA(stats);
    nStats = (int)statsDims[0];

    float *wg;
    if (!w || w == Py_None) {
        wg = 0;
    } else {
        PyArray_Descr* dtype = PyArray_DescrFromType(NPY_FLOAT32);
        //int _ndims;
        //npy_intp _dims;
        PyArrayObject* _w = (PyArrayObject*)PyArray_FromAny(w, dtype, 1, 1, NPY_ARRAY_CARRAY, NULL);
        //PyArray_GetArrayParamsFromObject(w,NULL,1,&dtype,&_ndims,&_dims,&_w,NULL);
        wg = (float*)PyArray_DATA(_w);
    }

    Data *nvg;
    if (!nv || nv == Py_None) {
        nvg = new Data[B];
        for (k=0; k<B; k++) nvg[k] = -9999.0;
    } else {
        PyArray_Descr* dtype = PyArray_DescrFromType(NPY_FLOAT32);
        //int _ndims;
        //npy_intp _dims;
        PyArrayObject* _nv = (PyArrayObject*)PyArray_FromAny(nv, dtype, 1, 1, NPY_ARRAY_CARRAY, NULL);
        //PyArray_GetArrayParamsFromObject(nv,NULL,1,&dtype,&_ndims,&_dims,&_nv,NULL);
        nvg = (Data*)PyArray_DATA(_nv);
    }

    RasterStats1D rs(ol_arr,oR,oC);
    rs.addRaster(ras_arr,B,R,C,nvg,row_idx,col_idx,wg);

    nObj = rs.getNObjects();

    int *cstats = new int[nStats];
    int tot_stats = 0;
    for (s=0; s<nStats; s++) {
        cstats[s] = rs.computeStatistics(0,stats_vec[s]);
        tot_stats += cstats[s];
    }

    float **zstats = new float*[nObj];
    for (t=0; t<nObj; t++) zstats[t] = new float[tot_stats+1];

    map<Label,float> tmp;
    map<Label,float>::iterator it;

    int cnt = 0;
    for (s=0; s<nStats; s++) {
        for (k=0; k<cstats[s]; k++) {
            tmp.clear();
            tmp = rs.getStat(0,k,stats_vec[s]);
            if (k==0 && s==0) {
                t = 0;
                for (it=tmp.begin();it!=tmp.end();it++) zstats[t++][cnt] = it->first;
                cnt++;
            }
            t = 0;
            for (it=tmp.begin();it!=tmp.end();it++) zstats[t++][cnt] = it->second;
            cnt++;
        }
    }

    npy_intp featDims = (npy_intp)tot_stats;
    PyObject *out_dict = PyDict_New();
    PyObject *key,*val;
    for (t=0;t<nObj;t++) {
        key = PyLong_FromLong((long)round(zstats[t][0]));
        val = PyArray_SimpleNewFromData(1,&featDims,NPY_FLOAT32,&zstats[t][1]);
        PyDict_SetItem(out_dict,key,val);
    }

    npy_intp py_nStats = (npy_intp)nStats;
    PyObject *out_cstats = PyArray_SimpleNewFromData(1,&py_nStats,NPY_INT32,cstats);

    delete [] zstats;

    return Py_BuildValue("OO",out_dict,out_cstats);

}

// MODULE INITIALIZATION
static PyMethodDef obiatoolsMethods[] = {
        {"version", obiatools_version, METH_VARARGS, "Gives version of the OBIATOOLS module."},
        //{"test", obiatools_test, METH_VARARGS, "Generic test function."},
        {"zonalstats", obiatools_zonalstats, METH_VARARGS, "Simple object-based zonal statistics extraction from single (possibly multi-band) rasters."},
        //{"zonalstatsv2", obiatools_zonalstats, METH_VARARGS, "Simple object-based zonal statistics extraction from single (possibly multi-band) rasters. Optimized."},
        {NULL, NULL, 0, NULL}
};

#ifdef PY3K
static struct PyModuleDef obiatoolsdef = {
    PyModuleDef_HEAD_INIT,
    "obiatools",
    "obia doc",
    -1,
    obiatoolsMethods
};

// module initializer for python3
PyMODINIT_FUNC PyInit_obiatools()
{
    PyObject* mod = PyModule_Create(&obiatoolsdef);
    PyModule_AddIntConstant(mod,"OSTAT_COUNT",OSTAT_COUNT);
    PyModule_AddIntConstant(mod,"OSTAT_SUM",OSTAT_SUM);
    PyModule_AddIntConstant(mod,"OSTAT_MEAN",OSTAT_MEAN);
    PyModule_AddIntConstant(mod,"OSTAT_MEDIAN",OSTAT_MEDIAN);
    PyModule_AddIntConstant(mod,"OSTAT_STD",OSTAT_STD);
    PyModule_AddIntConstant(mod,"OSTAT_MIN",OSTAT_MIN);
    PyModule_AddIntConstant(mod,"OSTAT_MAX",OSTAT_MAX);
    PyModule_AddIntConstant(mod,"OSTAT_RANGE",OSTAT_RANGE);
    PyModule_AddIntConstant(mod,"OSTAT_MINORITY",OSTAT_MINORITY);
    PyModule_AddIntConstant(mod,"OSTAT_MAJORITY",OSTAT_MAJORITY);
    PyModule_AddIntConstant(mod,"OSTAT_VARIETY",OSTAT_VARIETY);
    import_array();
    return mod;
}
#else
// module initializer for python2
PyMODINIT_FUNC initobiatools(void) {
    //(void) Py_InitModule("obiatools",obiatoolsMethods);
    PyObject* mod = Py_InitModule("obiatools",obiatoolsMethods);

    PyModule_AddIntConstant(mod,"OSTAT_COUNT",OSTAT_COUNT);
    PyModule_AddIntConstant(mod,"OSTAT_SUM",OSTAT_SUM);
    PyModule_AddIntConstant(mod,"OSTAT_MEAN",OSTAT_MEAN);
    PyModule_AddIntConstant(mod,"OSTAT_MEDIAN",OSTAT_MEDIAN);
    PyModule_AddIntConstant(mod,"OSTAT_STD",OSTAT_STD);
    PyModule_AddIntConstant(mod,"OSTAT_MIN",OSTAT_MIN);
    PyModule_AddIntConstant(mod,"OSTAT_MAX",OSTAT_MAX);
    PyModule_AddIntConstant(mod,"OSTAT_RANGE",OSTAT_RANGE);
    PyModule_AddIntConstant(mod,"OSTAT_MINORITY",OSTAT_MINORITY);
    PyModule_AddIntConstant(mod,"OSTAT_MAJORITY",OSTAT_MAJORITY);
    PyModule_AddIntConstant(mod,"OSTAT_VARIETY",OSTAT_VARIETY);

    import_array();
}
#endif
