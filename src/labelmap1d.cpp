/*
 * labelmap.cpp
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#include "labelmap1d.h"

LabelMap1D::LabelMap1D(Label* in, int r, int c, Label bg) {

	int i,j,cnt;
	Label tmp;

	rows = r;
	cols = c;

	background = bg;

	cnt = 0;

	pair<map<Label,int>::iterator,bool> ret;

	lmap = in;
	for (i=0;i<rows;i++) {
		for (j=0;j<cols;j++) {
			tmp = lmap[i*cols+j];
			if (tmp != background) {
				ret = rLabels.insert(pair<Label,int>(tmp,cnt));
				if (ret.second)
					labels.insert(pair<int,Label>(cnt++,tmp));
			}
		}
	}

	nObjects = (Label)labels.size();

}

LabelMap1D::~LabelMap1D() {

}
