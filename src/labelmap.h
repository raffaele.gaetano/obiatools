/*
 * labelmap.h
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#ifndef LABELMAP_H_
#define LABELMAP_H_

#include "obiatools.h"
#include <map>

using namespace std;

class LabelMap {

private:
	Label** lmap;

public:
	int rows,cols;
	Label background;
	map<int, Label> labels;
	map<Label,int> rLabels;
	Label nObjects;

	LabelMap(Label** in, int r, int c, Label bg = 0);
	~LabelMap();

	inline Label& operator()(int i, int j) {
		return lmap[i][j];
	}

};

#endif /* LABELMAP_H_ */
