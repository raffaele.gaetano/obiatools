/*
 * rasterdata.cpp
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#include "rasterdata1d.h"

RasterData1D::RasterData1D(Data* in, int b, int r, int c, Data* nv, int oR, int oC, int* row_idx, int* col_idx) {

	int k;

	bands = b;
	rows = r;
	cols = c;
	ras = in;

	novalues = nv;

	oRows = oR;
	oCols = oC;

	rowIndices = row_idx;
	colIndices = col_idx;

	for (k=0;k<bands;k++) selectedBands.push_back(k);

	weights = 0;

}

RasterData1D::~RasterData1D() {

}

void RasterData1D::selectBands(vector<int> sb) {

	size_t t;
	selectedBands.clear();
	for (t=0;t<sb.size();t++) selectedBands.push_back(sb[t]);


}

void RasterData1D::deselectBands() {

	int t;
	selectedBands.clear();
	for (t=0;t<bands;t++) selectedBands[t] = t;

}

void RasterData1D::setWeights(float* w) {

	weights = w;

}
