/*
 * labelmap.cpp
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#include "labelmap.h"

LabelMap::LabelMap(Label** in, int r, int c, Label bg) {

	int i,j,cnt;

	rows = r;
	cols = c;

	background = bg;

	cnt = 0;

	pair<map<Label,int>::iterator,bool> ret;

	lmap = new Label*[rows];
	for (i=0;i<rows;i++) {
		lmap[i] = new Label[cols];
		for (j=0;j<cols;j++) {
			lmap[i][j] = in[i][j];
			if (lmap[i][j] != background) {
				ret = rLabels.insert(pair<Label,int>(lmap[i][j],cnt));
				if (ret.second)
					labels.insert(pair<int,Label>(cnt++,lmap[i][j]));
			}
		}
	}

	nObjects = (Label)labels.size();

}

LabelMap::~LabelMap() {

	int i;
	for (i=0;i<rows;i++) {
		delete [] lmap[i];
	}
	delete [] lmap;

}
