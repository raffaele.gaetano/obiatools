/*
 * dataconverter.h
 *
 *  Created on: 21 sept. 2015
 *      Author: gaetano
 */

#ifndef DATACONVERTER_H_
#define DATACONVERTER_H_

template <typename T, typename TI>
T** array_1to2d(T* x, TI* dims) {

	TI i,j,c;
	c = 0;

	T** y = new T*[dims[0]];
	for (i=0;i<dims[0];i++) {
		y[i] = new T[dims[1]];
		for (j=0;j<dims[1];j++) {
			y[i][j] = x[c++];
		}
	}

	return y;

}

template <typename T, typename TI>
T* array_2to1d(T** x, TI* dims) {

	TI i,j,c;
	c = 0;

	T* y = new T[dims[0]*dims[1]];
	for (i=0;i<dims[0];i++) {
		for (j=0;j<dims[1];j++) {
			y[c++] = x[i][j];
		}
	}

	return y;

}

template <typename T, typename TI>
T*** array_1to3d(T* x, TI* dims) {

	TI i,j,k,c;
	c = 0;

	T*** y = new T**[dims[0]];
	for (k=0;k<dims[0];k++) {
		y[k] = new T*[dims[1]];
		for (i=0;i<dims[1];i++) {
			y[k][i] = new T[dims[2]];
			for (j=0;j<dims[2];j++) {
				y[k][i][j] = x[c++];
			}
		}
	}

	return y;

}

template <typename T, typename TI>
T* array_3to1d(T*** x, TI* dims) {

	TI i,j,k,c;
	c = 0;

	T* y = new T[dims[0]*dims[1]*dims[2]];
	for (k=0;k<dims[0];k++) {
		for (i=0;i<dims[1];i++) {
			for (j=0;j<dims[2];j++) {
				y[c++] = x[k][i][j];
			}
		}
	}

	return y;

}

template <typename T, typename TI>
void del_array_3d(T*** x, TI* dims) {

	TI i,k;

	for (k=0;k<dims[0];k++) {
		for (i=0;i<dims[1];i++) {
			delete [] x[k][i];
		}
		delete [] x[k];
	}
	delete [] x;
}

template <typename T, typename TI>
void del_array_2d(T** x, TI* dims) {

	TI i;

	for (i=0;i<dims[0];i++) {
		delete [] x[i];
	}
	delete [] x;

}

#endif /* DATACONVERTER_H_ */
