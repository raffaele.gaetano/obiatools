/*
 * rasterstats.h
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#ifndef RASTERSTATS1D_H_
#define RASTERSTATS1D_H_

#define OSTAT_NUM_STATS 11

#define OSTAT_COUNT 0
#define OSTAT_SUM 1
#define OSTAT_MEAN 2
#define OSTAT_MEDIAN 3
#define OSTAT_STD 4
#define OSTAT_MIN 5
#define OSTAT_MAX 6
#define OSTAT_RANGE 7
#define OSTAT_MINORITY 8
#define OSTAT_MAJORITY 9
#define OSTAT_VARIETY 10

#define OSTAT_TEST_FEATURE -1

#include "labelmap1d.h"
#include "rasterdata1d.h"
#include <vector>
#include <map>

//DEBUG
//#include <Python.h>
//-----

using namespace std;

class RasterStats1D {

private:
	LabelMap1D *objMap;
	vector<RasterData1D*> rasters;
	vector<float***> stats;
	vector<int*> stats_sizes;
	vector< map<Label,long**> > histograms;
	vector< map<Label,Data**> > histBounds;
	vector< map<Label,int*> > histNBins;

	inline int findBin(Data d, int nBins, Data* bounds) {
		int idx = 0;
		while ((idx < (nBins-1)) && (bounds[idx+1] < d)) idx++;
		return idx;
	}

	inline int medianIdx(long* hist, int nBins) {
		float perc = 0.0;
		int idx = 0;
		while (idx < nBins && perc < 0.5)
			perc += (float)hist[idx++] / (float)hist[nBins];
		return idx - 1;
	}

	void computeMean(int rasnum);
	void computeStd(int rasnum);

	void computeMin(int rasnum);
	void computeMax(int rasnum);
	void computeRange(int rasnum);

	void computeHistograms(int rasnum, float binsize = 1.0);
	void computeMajority(int rasnum, float binsize = 1.0);
	void computeMinority(int rasnum, float binsize = 1.0);
	void computeVariety(int rasnum, float binsize = 1.0);
	void computeMedian(int rasnum, float binsize = 1.0);


public:
	RasterStats1D(Label* obj, int r, int c, Label bg = 0);
	~RasterStats1D();
	void addRaster(Data* in, int b, int r, int c, Data* nv, int* row_idx, int* col_idx, float* weights = 0);

	int computeStatistics(int rasnum, int statnum, float param = 1.0);
	int computeStatistics(int rasnum, int statnum, vector<int> sb, float param = 1.0);

	map<Label,float> getStat(int rasnum, int band, int statnum);

	Label getNObjects() { return objMap->nObjects; }

};

#endif /* RASTERSTATS_H_ */
