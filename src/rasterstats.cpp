/*
 * rasterstats.cpp
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#include "rasterstats.h"
#include <cmath>
#include <cfloat>
#include <algorithm>

RasterStats::RasterStats(Label** obj, int r, int c, Label bg) {

	objMap = new LabelMap(obj,r,c,bg);

}

RasterStats::~RasterStats() {

	int k,s,t;
	size_t u;

	for (k=0;k<(int)rasters.size();k++) {
		for (s=0;s<OSTAT_NUM_STATS;s++) {
			if (stats_sizes[k][s] > 0) {
				for (t=0;t<objMap->nObjects;t++) delete [] stats[k][s][t];
				delete [] stats[k][s];
			}
		}
		delete [] stats[k];
		delete [] stats_sizes[k];

		if (histNBins[k].size() > 0) {

			for (t=0;t<objMap->nObjects;t++) {
				for (u=0;u<rasters[k]->selectedBands.size();u++) {
					delete [] histograms[k][t][u];
					delete [] histBounds[k][t][u];
				}
				delete [] histograms[k][t];
				delete [] histBounds[k][t];
				delete [] histNBins[k][t];
			}

		}

		delete rasters[k];
	}

	delete objMap;

}

void RasterStats::addRaster(Data*** in, int b, int r, int c, int* row_idx, int* col_idx, float** weights) {

	int s;
	//float rrf,crf;

	//rrf = (float)objMap->rows / (float)r;
	//crf = (float)objMap->cols / (float)c;

	RasterData *rs = new RasterData(in,b,r,c,objMap->rows,objMap->cols,row_idx,col_idx);
	if (weights) rs->setWeights(weights);
	rasters.push_back(rs);

	float ***r_stats = new float**[OSTAT_NUM_STATS];
	int *r_stats_sizes = new int[OSTAT_NUM_STATS];
	for (s=0;s<OSTAT_NUM_STATS;s++) r_stats_sizes[s] = 0;
	stats.push_back(r_stats);
	stats_sizes.push_back(r_stats_sizes);

	map<Label,long**> hst;
	map<Label,Data**> bnd;
	map<Label,int*> nbn;
	histograms.push_back(hst);
	histBounds.push_back(bnd);
	histNBins.push_back(nbn);

}

int RasterStats::computeStatistics(int rasnum, int statnum, float param) {

	if (stats_sizes[rasnum][statnum] > 0) return stats_sizes[rasnum][statnum];

	switch (statnum) {
	case OSTAT_COUNT:
		computeMean(rasnum);
		return stats_sizes[rasnum][OSTAT_COUNT];
		break;
	case OSTAT_SUM:
		computeMean(rasnum);
		return stats_sizes[rasnum][OSTAT_SUM];
		break;
	case OSTAT_MEAN:
		computeMean(rasnum);
		return stats_sizes[rasnum][OSTAT_MEAN];
		break;
	case OSTAT_STD:
		computeStd(rasnum);
		return stats_sizes[rasnum][OSTAT_STD];
		break;
	case OSTAT_MIN:
		computeMin(rasnum);
		return stats_sizes[rasnum][OSTAT_MIN];
		break;
	case OSTAT_MAX:
		computeMax(rasnum);
		return stats_sizes[rasnum][OSTAT_MAX];
		break;
	case OSTAT_RANGE:
		computeRange(rasnum);
		return stats_sizes[rasnum][OSTAT_RANGE];
		break;
	case OSTAT_MAJORITY:
		computeMajority(rasnum, param);
		return stats_sizes[rasnum][OSTAT_MAJORITY];
		break;
	case OSTAT_MINORITY:
		computeMinority(rasnum, param);
		return stats_sizes[rasnum][OSTAT_MINORITY];
		break;
	case OSTAT_VARIETY:
		computeVariety(rasnum, param);
		return stats_sizes[rasnum][OSTAT_VARIETY];
		break;
	case OSTAT_MEDIAN:
		computeMedian(rasnum, param);
		return stats_sizes[rasnum][OSTAT_MEDIAN];
		break;
	case OSTAT_TEST_FEATURE:
		computeHistograms(rasnum, param);
		return -1;
		break;
	default:
		return -1;
	}

}

int RasterStats::computeStatistics(int rasnum, int statnum, vector<int> sb, float param) {

	rasters[rasnum]->selectBands(sb);
	int ret = computeStatistics(rasnum,statnum,param);
	rasters[rasnum]->deselectBands();
	return ret;

}

void RasterStats::computeMean(int rn) {

	int t,k,i,j;

	float **counts, **sums, **means;

	int B = rasters[rn]->selectedBands.size();

	counts = new float*[objMap->nObjects];
	sums = new float*[objMap->nObjects];
	means = new float*[objMap->nObjects];
	for (t=0;t<objMap->nObjects;t++) {
		counts[t] = new float[1];
		counts[t][0] = 0.0;
		sums[t] = new float[B];
		for (k=0;k<B;k++) sums[t][k] = 0.0;
		means[t] = new float[B];
	}

	float w;
	Data d;
	Label objIdx;
	Label curr;
	for (k=0;k<B;k++) {
		for (i=0;i<objMap->rows;i++) {
			for (j=0;j<objMap->cols;j++) {
				curr = (*objMap)(i,j);
				if (curr != objMap->background) {
					d = (*rasters[rn])(k,i,j,&w);
					objIdx = objMap->rLabels[curr];
					sums[objIdx][k] += w*d;
					if (k==0) counts[objIdx][0] += w;
				}
			}
		}
		for (t=0;t<objMap->nObjects;t++) {
			means[t][k] = sums[t][k] / counts[t][0];
		}
	}

	stats_sizes[rn][OSTAT_COUNT] = 1;
	stats[rn][OSTAT_COUNT] = counts;
	stats_sizes[rn][OSTAT_SUM] = B;
	stats[rn][OSTAT_SUM] = sums;
	stats_sizes[rn][OSTAT_MEAN] = B;
	stats[rn][OSTAT_MEAN] = means;

}

void RasterStats::computeStd(int rn) {

	int t,k,i,j;

	if (stats_sizes[rn][OSTAT_MEAN] == 0) computeMean(rn);

	float **sums = new float*[objMap->nObjects];
	float **stds = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();

	for (t=0;t<objMap->nObjects;t++) {
		sums[t] = new float[B];
		for (k=0;k<B;k++) sums[t][k] = 0.0;
		stds[t] = new float[B];
	}

	Label curr;
	Label objIdx;
	Data d;
	float w;
	for (k=0;k<B;k++) {
		for (i=0;i<objMap->rows;i++) {
			for (j=0;j<objMap->cols;j++) {
				curr = (*objMap)(i,j);
				if (curr != objMap->background) {
					d = (*rasters[rn])(k,i,j,&w);
					objIdx = objMap->rLabels[curr];
					sums[objIdx][k] += w * pow(d - stats[rn][OSTAT_MEAN][objIdx][k],2);
				}
			}
		}
		for (t=0;t<objMap->nObjects;t++) {
			stds[t][k] = sqrt(sums[t][k] / stats[rn][OSTAT_COUNT][t][0]);
		}
	}

	stats_sizes[rn][OSTAT_STD] = B;
	stats[rn][OSTAT_STD] = stds;

	for (t=0;t<objMap->nObjects;t++) delete [] sums[t];
	delete [] sums;

}

void RasterStats::computeMin(int rn) {

	int t,k,i,j;

	float** mins = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();

	for (t=0;t<objMap->nObjects;t++) {
		mins[t] = new float[B];
		for (k=0; k<B; k++) mins[t][k] = FLT_MAX;
	}

	Data d;
	Label curr;
	Label objIdx;
	for (k=0;k<B;k++) {
		for (i=0;i<objMap->rows;i++) {
			for (j=0;j<objMap->cols;j++) {
				curr = (*objMap)(i,j);
				if (curr != objMap->background) {
					d = (*rasters[rn])(k,i,j);
					objIdx = objMap->rLabels[curr];
					if (d < mins[objIdx][k]) mins[objIdx][k] = d;
				}
			}
		}
	}

	stats_sizes[rn][OSTAT_MIN] = B;
	stats[rn][OSTAT_MIN] = mins;

}

void RasterStats::computeMax(int rn) {

	int t,k,i,j;

	float** maxs = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();

	for (t=0;t<objMap->nObjects;t++) {
		maxs[t] = new float[B];
		for (k=0; k<B; k++) maxs[t][k] = FLT_MIN;
	}

	Data d;
	Label curr;
	Label objIdx;
	for (k=0;k<B;k++) {
		for (i=0;i<objMap->rows;i++) {
			for (j=0;j<objMap->cols;j++) {
				curr = (*objMap)(i,j);
				if (curr != objMap->background) {
					d = (*rasters[rn])(k,i,j);
					objIdx = objMap->rLabels[curr];
					if (d > maxs[objIdx][k]) maxs[objIdx][k] = d;
				}
			}
		}
	}

	stats_sizes[rn][OSTAT_MAX] = B;
	stats[rn][OSTAT_MAX] = maxs;

}

void RasterStats::computeRange(int rn) {

	int t,k;

	float** ranges = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();

	for (t=0;t<objMap->nObjects;t++)
		ranges[t] = new float[B];

	if (stats_sizes[rn][OSTAT_MAX] == 0) computeMax(rn);
	if (stats_sizes[rn][OSTAT_MIN] == 0) computeMin(rn);

	for (k=0;k<B;k++) {
		for (t=0;t<objMap->nObjects;t++) {
			ranges[t][k] = stats[rn][OSTAT_MAX][t][k] - stats[rn][OSTAT_MIN][t][k];
		}
	}

	stats_sizes[rn][OSTAT_RANGE] = B;
	stats[rn][OSTAT_RANGE] = ranges;

}

void RasterStats::computeHistograms(int rn, float bs) {

	int i,j,t,k,s;
	float minH;

	int B = rasters[rn]->selectedBands.size();

	if (stats_sizes[rn][OSTAT_MAX] == 0) computeMax(rn);
	if (stats_sizes[rn][OSTAT_MIN] == 0) computeMin(rn);

	int **R = new int*[objMap->nObjects];
	Data ***bounds = new Data**[objMap->nObjects];
	long ***tmp = new long**[objMap->nObjects];

	for (t=0;t<objMap->nObjects;t++) {
		tmp[t] = new long*[B];
		bounds[t] = new Data*[B];
		R[t] = new int[B];
		for (k=0;k<B;k++) {
			R[t][k] = (int)round((float)(stats[rn][OSTAT_MAX][t][k] - stats[rn][OSTAT_MIN][t][k] + 1) / bs);
			tmp[t][k] = new long[R[t][k]+1]; // idx R for total
			bounds[t][k] = new Data[R[t][k]+1]; // R bins --> R+1 bounds
			minH = (float)(stats[rn][OSTAT_MIN][t][k])-bs/2;
			for (s=0;s<=R[t][k];s++) {
				bounds[t][k][s] = minH + s * bs;
				tmp[t][k][s] = 0;
			}
		}
	histBounds[rn][t] = bounds[t];
	histNBins[rn][t] = R[t];
	}

	Data d;
	Label curr;
	Label objIdx;
	int cb;
	for (k=0;k<B;k++) {
		for (i=0;i<objMap->rows;i++) {
			for (j=0;j<objMap->cols;j++) {
				curr = (*objMap)(i,j);
				if (curr != objMap->background) {
					d = (*rasters[rn])(k,i,j);
					objIdx = objMap->rLabels[curr];
					cb = findBin(d,R[objIdx][k],bounds[objIdx][k]);
					tmp[objIdx][k][cb]++;
					tmp[objIdx][k][R[objIdx][k]]++;
				}
			}
		}
	}

	for (t=0;t<objMap->nObjects;t++) {
		histograms[rn][t] = tmp[t];
	}

	delete [] tmp;
	delete [] R;
	delete [] bounds;

}

void RasterStats::computeMajority(int rn, float bs) {

	int t,k;

	if (histNBins[rn].size() == 0) computeHistograms(rn,bs);

	float** maj = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();
	int idx;

	for (t=0;t<objMap->nObjects;t++) {
		maj[t] = new float[B];
		for (k=0;k<B;k++) {
			idx = distance(histograms[rn][t][k],max_element(histograms[rn][t][k],histograms[rn][t][k]+histNBins[rn][t][k]));
			maj[t][k] = (float)(histBounds[rn][t][k][idx+1] + histBounds[rn][t][k][idx]) / 2;
		}
	}

	stats_sizes[rn][OSTAT_MAJORITY] = B;
	stats[rn][OSTAT_MAJORITY] = maj;
}

void RasterStats::computeMinority(int rn, float bs) {

	int t,k;

	if (histNBins[rn].size() == 0) computeHistograms(rn,bs);

	float** minr = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();
	int idx;

	for (t=0;t<objMap->nObjects;t++) {
		minr[t] = new float[B];
		for (k=0;k<B;k++) {
			idx = distance(histograms[rn][t][k],min_element(histograms[rn][t][k],histograms[rn][t][k]+histNBins[rn][t][k]));
			minr[t][k] = (float)(histBounds[rn][t][k][idx+1] + histBounds[rn][t][k][idx]) / 2;
		}
	}

	stats_sizes[rn][OSTAT_MINORITY] = B;
	stats[rn][OSTAT_MINORITY] = minr;

}

void RasterStats::computeVariety(int rn, float bs) {

	int t,k,b;

	if (histNBins[rn].size() == 0) computeHistograms(rn,bs);

	float** vary = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();

	for (t=0;t<objMap->nObjects;t++) {
		vary[t] = new float[B];
		for (k=0;k<B;k++) {
			vary[t][k] = 0.0;
			for (b=0;b<histNBins[rn][t][k];b++)
				vary[t][k] += (float)(histograms[rn][t][k][b] > 0);
		}
	}

	stats_sizes[rn][OSTAT_VARIETY] = B;
	stats[rn][OSTAT_VARIETY] = vary;

}

void RasterStats::computeMedian(int rn, float bs) {

	int t,k;

	if (histNBins[rn].size() == 0) computeHistograms(rn,bs);

	float** medi = new float*[objMap->nObjects];

	int B = rasters[rn]->selectedBands.size();
	int idx;

	for (t=0;t<objMap->nObjects;t++) {
		medi[t] = new float[B];
		for (k=0;k<B;k++) {
			idx = medianIdx(histograms[rn][t][k],histNBins[rn][t][k]);
			medi[t][k] = (float)(histBounds[rn][t][k][idx+1] + histBounds[rn][t][k][idx]) / 2;
		}
	}

	stats_sizes[rn][OSTAT_MEDIAN] = B;
	stats[rn][OSTAT_MEDIAN] = medi;

}

map<Label,float> RasterStats::getStat(int rasnum, int band, int statnum) {

	int N,t;
	map<Label,float> out;

	N = objMap->nObjects;

	if (stats_sizes[rasnum][statnum] > 0) {
		for (t=0; t<N; t++) {
			out.insert(pair<Label,float>(objMap->labels[t],stats[rasnum][statnum][t][band]));
		}
	}

	return out;

}

