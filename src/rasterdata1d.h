/*
 * rasterdata.h
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#ifndef RASTERDATA1D_H_
#define RASTERDATA1D_H_

#include "obiatools.h"
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

class RasterData1D {

private:
	Data* ras;
	float *weights;
	int bands;
	int rows,cols;
	Data* novalues;
	int oRows, oCols;
	int *rowIndices, *colIndices;

public:

	vector<int> selectedBands;

	RasterData1D(Data* in, int b, int r, int c, Data* nv, int oRows, int oCols, int* row_idx, int* col_idx);
	~RasterData1D();

	inline Data& operator()(int k, int i, int j, float* w = 0) {
		int x,y;
		x = rowIndices[i];
		y = colIndices[j];
		if (w) {
			if (!weights) {
				*w = 1.0;
			} else {
				*w = weights[x*cols+y];
			}
		}
		return ras[selectedBands[k]*rows*cols+x*cols+y];
	}

	inline float _weight(int i, int j) {
		int x,y;
		if (!weights) return 1.0;
		else {
			x = rowIndices[i];
			y = colIndices[j];
			return weights[x*cols+y];
		}
	}

	inline Data getNoValue(int b) {
		if (novalues) {
			return novalues[b];
		}
		else return -9999.0F;
	}

	void setWeights(float* w);

	void selectBands(vector<int> sb);
	void deselectBands();

};

#endif /* RASTERDATA1D_H_ */
