/*
 * labelmap.h
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#ifndef LABELMAP1D_H_
#define LABELMAP1D_H_

#include "obiatools.h"
#include <map>

using namespace std;

class LabelMap1D {

private:
	Label* lmap;

public:
	int rows,cols;
	Label background;
	map<int, Label> labels;
	map<Label,int> rLabels;
	Label nObjects;

	LabelMap1D(Label* in, int r, int c, Label bg = 0);
	~LabelMap1D();

	inline Label& operator()(int i, int j) {
		return lmap[i*cols+j];
	}

};

#endif /* LABELMAP_H_ */
