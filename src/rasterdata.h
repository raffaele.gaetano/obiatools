/*
 * rasterdata.h
 *
 *  Created on: 13 juil. 2016
 *      Author: gaetano
 */

#ifndef RASTERDATA_H_
#define RASTERDATA_H_

#include "obiatools.h"
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

class RasterData {

private:
	Data*** ras;
	float **weights;
	int bands;
	int rows,cols;
	float rResolutionFactor,cResolutionFactor;
	int rowOffset, colOffset;
	int oRows, oCols;
	int *rowIndices, *colIndices;

public:

	vector<int> selectedBands;

	//RasterData(Data*** in, int b, int r, int c, float rrf = 1.0, int roff = 0, float crf = -1.0, int coff = -1);
	RasterData(Data*** in, int b, int r, int c, int oRows, int oCols, int* row_idx, int* col_idx);
	~RasterData();

	/*
	inline Data& operator()(int k, int i, int j, float* w = 0) {
		int x,y;
		x = (int)floor(rResolutionFactor * (float)(i+rowOffset));
		y = (int)floor(cResolutionFactor * (float)(j+colOffset));
		// *** check performances! ***
		//x = x == rows ? rows-1 : x;
		//y = y == cols ? cols-1 : y;
		// ***************************
		if (w) {
			if (!weights) {
				*w = 1.0;
			} else {
				*w = weights[x][y];
			}
		}
		return ras[selectedBands[k]][x][y];
	}
	*/

	inline Data& operator()(int k, int i, int j, float* w = 0) {
		int x,y;
		x = rowIndices[i];
		y = colIndices[j];
		if (w) {
			if (!weights) {
				*w = 1.0;
			} else {
				*w = weights[x][y];
			}
		}
		return ras[selectedBands[k]][x][y];
	}

	/*
	inline float _weight(int i, int j) {
		int x,y;
		if (!weights) return 1.0;
		else {
			x = (int)floor(rResolutionFactor * (float)(i+rowOffset));
			y = (int)floor(cResolutionFactor * (float)(j+colOffset));
			// *** check performances! ***
			//x = x == rows ? rows-1 : x;
			//y = y == cols ? cols-1 : y;
			return weights[x][y];
		}
	}
	*/

	inline float _weight(int i, int j) {
		int x,y;
		if (!weights) return 1.0;
		else {
			x = rowIndices[i];
			y = colIndices[j];
			return weights[x][y];
		}
	}

	void setWeights(float** w);

	void selectBands(vector<int> sb);
	void deselectBands();

};

#endif /* RASTERDATA_H_ */
