export NUMPY_INCLUDE=/usr/lib/python3/dist-packages/numpy/core/include/numpy
export PYTHON_INCLUDE=/usr/include/python3.6
export PYTHON_LIB=python3.6
export PYTHON_LIB_PATH=/usr/lib
export PYTHON_LD_EXT=so
export PYTHON_LD_INSTALLDIR=$HOME/moringa/sitsproc_c_modules
export PYTHON_CPY_LINKERFLAGS=
