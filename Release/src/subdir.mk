################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/labelmap.cpp \
../src/labelmap1d.cpp \
../src/main.cpp \
../src/rasterdata.cpp \
../src/rasterdata1d.cpp \
../src/rasterstats.cpp \
../src/rasterstats1d.cpp 

OBJS += \
./src/labelmap.o \
./src/labelmap1d.o \
./src/main.o \
./src/rasterdata.o \
./src/rasterdata1d.o \
./src/rasterstats.o \
./src/rasterstats1d.o 

CPP_DEPS += \
./src/labelmap.d \
./src/labelmap1d.d \
./src/main.d \
./src/rasterdata.d \
./src/rasterdata1d.d \
./src/rasterstats.d \
./src/rasterstats1d.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DMS_WIN64 -I"$(PYTHON_INCLUDE)" -I"$(NUMPY_INCLUDE)" -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


